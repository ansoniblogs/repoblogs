package com.captton.clases;

public class Usuario {

	private String nombre;
	private String email;

	public Usuario(String nombre, String email) {
		super();
		this.nombre = nombre;
		this.email = email;
	}
	public Usuario() {		
	}
		
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void comentar(String contenido, Post post) {
		Comentario aux = dameComentario(this, contenido, post);
		post.getComentarios().add(aux);
	}
	
	public Comentario dameComentario(Usuario user,String contenido, Post post) {
		return new Comentario(user, contenido, post);
	}
	
	public Post crearPost(String titulo, String desc, Blog blog) {
		Post aux = damePost(titulo, this, desc);
		blog.Posts.add(aux);
		return aux;
	}
	
	
	public Post damePost(String titulo, Usuario user, String contenido) {
		return new Post(titulo, user, contenido);
	}

}
	

