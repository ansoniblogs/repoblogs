package com.captton.clases;

import java.util.ArrayList;

public class Blog {
	
	protected ArrayList<Post> Posts;
	
	public Blog() {
		this.Posts = new ArrayList<Post>();
	}
	
	public void mostrarP() {
		
		for(Post p: this.Posts) {
			System.out.println("Titulo: "+p.getTitulo()+"\n"+p.getContenido()+"\n"+"By: "+p.getUsuario().getNombre()+" <"+p.getUsuario().getEmail()+">");
			System.out.println("\nComentarios:");
			p.mostrarComentarios();
			System.out.println("----------FIN DELPOST-----------\n");
		}
	}
	
	
}
