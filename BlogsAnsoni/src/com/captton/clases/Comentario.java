package com.captton.clases;


public class Comentario {
	private Usuario user;
	private String contenido;
	protected Post postAlQPertenece;
	
	public Comentario(Usuario user, String contenido, Post post) {
		this.user = user;
		this.contenido = contenido;
		this.postAlQPertenece = post;
	}
	
	public Comentario(Usuario user, String contenido) {
		this.user = user;
		this.contenido = contenido;
	}
	

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	
	public String getUser() {
		return user.getNombre();
	}
	
	
}
