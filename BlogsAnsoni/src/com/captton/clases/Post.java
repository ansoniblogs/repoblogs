package com.captton.clases;

import java.util.ArrayList;

public class Post {

	  private String titulo;
	  private Usuario usuario;
	  private String contenido;
	  private ArrayList<Comentario> comentarios;
	  
	  
	  public Post(String titulo, Usuario usuario, String contenido) {
		super();
		this.titulo = titulo;
		this.usuario = usuario;
		this.contenido = contenido;
		this.comentarios = new ArrayList<Comentario>();
		
	}


	public String getTitulo() {
		return titulo;
	}


	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}


	public Usuario getUsuario() {
		return usuario;
	}


	public void setUsuario(Usuario usuario) {
		this.usuario = usuario; 
	}

	

	public ArrayList<Comentario> getComentarios() {
		return comentarios;
	}


	public void setComentarios(ArrayList<Comentario> comentarios) {
		this.comentarios = comentarios;
	}


	public String getContenido() {
		return contenido;
	}


	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	

	
	public void mostrarComentarios() {
		for(Comentario aux: this.comentarios) {
			System.out.println(aux.getUser()+" escribio: "+aux.getContenido());
		}
	}
	
	public Post damePost(String titulo, Usuario user, String contenido) {
		return new Post(titulo, user, contenido);
	}
		
}
